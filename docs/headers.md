# Site Headers

There are three main types of values we inject in the `head` of the site. For the most part, we can manage these globally through `nuxt.config.js` as explained in the [docs](https://nuxtjs.org/docs/2.x/features/meta-tags-seo). We have a default set of global settings, but each page can be more specifically configured using the [local settings](https://nuxtjs.org/docs/2.x/features/meta-tags-seo#local-settings) in its single file component `script` tag.

## Meta tags

Follow the [meta tags and SEO](https://nuxtjs.org/docs/2.x/features/meta-tags-seo) conventions set out by the Nuxt documentation when writing meta tags either globally or locally. Include a unique `hid` identifier, and then add the appropriate `name`, `content`, or other values as required. 

Where possible, we recommend using the same value for `hid` and `name`.

## External Resources

You can manage linked resources and external scripts through `nuxt.config.js` as well, as [documented by Nuxt](https://nuxtjs.org/docs/2.x/features/meta-tags-seo#external-resources). 

A few notes on this approach: 

1. It may not be practical or even desirable to convert third party code snippets into TypeScript. In these cases, feel free to use the `.js` file extension, and suppress ES lint warnings that crop up. It's better to stick to the third party boilerplate than to adapt it for our process. It will make troubleshooting easier if we stick to default installation instructions.
1. Third party code should probably be loaded only on the client side, since it often wants to hook into the `window` default global object that's not available at build time. Prefer the [object syntax for specifcying client side code in Nuxt](https://nuxtjs.org/docs/2.x/directory-structure/plugins#client-or-server-side-only) over the implicit [filename convention](https://nuxtjs.org/docs/2.x/directory-structure/plugins#name-conventional-plugin).
1. Even with the modifications above, you may have to tweak certain things. As an example, in `plugins/oneTrust.js`, we write: 

```js
window.OptanonWrapper = function() {}
```

Whereas the OneTrust documentation usually asks for users to add a script tag like: 

```html
<script>
  function OptanonWrapper() {}
</script>
```

Which assumes we have the ability to write `script` tags in a document, which we can't do direclty via plugins. 

## Disabling Sanitizers

Sometimes you can't avoid injecting a `script` via the `nuxt.config.js` or other `head` values in specific pages. You will have to disable the JavaScript sanitization. If that's the case, follow the example for our Schema.org values:

Our Schema.org metadata requires us to skip sanitizing its `innerHTML` value since we want to set it in `nuxt.config.js`, but it needs to render raw JSON values in a `script` tag. In order to do so, we've explicitly disabled sanitization using the `schemaOrg` id, [as recommended by Nuxt in the Vue meta documentation](https://vue-meta.nuxtjs.org/api/#dangerouslydisablesanitizersbytagid).

## Notes about specific `head` values
### Content Security Policy

We use the `http-equiv` version of a [content security policy](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP), found in the `contentSecurityPolicy` meta tag.

### Schema.org

[Asked Hanif to provide more context for this](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1483#note_656448568).

### OneTrust

[Asked Laura for documentation about our OneTrust implementation](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/1483#note_656452836).

### Google Tag Manager

Managed with the [`@nuxtjs/gtm`](https://www.npmjs.com/package/@nuxtjs/gtm) package. That package also manages the `dataLayer` setup for us, so there's no need for a manual script tag to do so anymore. 

### Favicon

You can find the favicon files in `/nuxt-images/ico/`, and we link to them the same way that `www-gitlab-com` has done in its [head partial](https://gitlab.com/gitlab-com/www-gitlab-com/blob/8883622870fb77de2e9be68309b1c7c78757e9fa/source/includes/layout/head.html.haml) as of the time of writing this document.
