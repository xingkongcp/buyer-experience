---
  title: What is CI/CD?
  description: Continuous Integration and Continuous Delivery (CICD) are terms used to describe a process where multiple changes are made to a codebase simultaneously. Learn more!
  old_topics_header:
    data:
      title: What is CI/CD?
      block:
        - metadata:
            id_tag: continuous-integration
          text: |
              Automate your software development workflows and deploy better quality code, more often. Using a continuous and iterative process to build, test, and deploy helps avoid bugs and code failures.
          link_text: Download the CICD Ebook Now
          link_href:  /resources/ebook-fuel-growth-cicd/
          data_ga_name: CI/CD webcast
          data_ga_location: header
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: CI CD
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: CI/CD explained
          href: "#ci-cd-explained"
          data_ga_name: ci cd explained
          data_ga_location: side-navigation
          variant: primary
        - text: CI/CD fundamentals
          href: "#ci-cd-fundamentals"
          data_ga_name: ci cd fundamentals
          data_ga_location: side-navigation
        - text: The benefits of CI/CD implementation
          href: "#the-benefits-of-ci-cd-implementation"
          data_ga_name: the benefits of ci cd implementation
          data_ga_location: side-navigation
        - text: Why GitLab CI/CD?
          href: "#why-git-lab-ci-cd"
          data_ga_name: why git lab ci cd
          data_ga_location: side-navigation
        - text: Build, test, deploy, and monitor your code from a single application
          href: "#build-test-deploy-and-monitor-your-code-from-a-single-application"
          data_ga_name: build test deploy and monitor your code from a single application
          data_ga_location: side-navigation
    hyperlinks:
      text: "More on this topic"
      data:
        - text: Continuous integration pipelines
          href: /topics/ci-cd/cicd-pipeline/
          data_ga_location: side-navigation
          data_ga_name: Continuous integration pipelines
          variant: tertiary
          icon: true
        - text: Benefits of continuous integration
          href: /topics/ci-cd/benefits-continuous-integration/
          data_ga_location: side-navigation
          data_ga_name: Benefits of continuous integration
          variant: tertiary
          icon: true
        - text: Implement continuous integration
          href: /topics/ci-cd/implement-continuous-integration/
          data_ga_location: side-navigation
          data_ga_name: Implement continuous integration
          variant: tertiary
          icon: true
        - text: Continuous integration best practices
          href: /topics/ci-cd/continuous-integration-best-practices/
          data_ga_location: side-navigation
          data_ga_name: Continuous integration best practice
          variant: tertiary
          icon: true
    content:
      - name: 'copy'
        data:
          block:
            - header: CI/CD explained
              hide_horizontal_rule: true
              column_size: 8
              text: |
                CI/CD falls under DevOps (the joining of development and operations teams) and combines the practices of continuous integration and continuous delivery. CI/CD automates much or all of the manual human intervention traditionally needed to get new code from a commit into production, encompassing the build, test (including integration tests, unit tests, and regression tests), and deploy phases, as well as infrastructure provisioning. With a CI/CD pipeline, development teams can make changes to code that are then automatically tested and pushed out for delivery and deployment. [Get CI/CD right](/blog/2020/07/06/beginner-guide-ci-cd/) and downtime is minimized and code releases happen faster.

                CI/CD is an essential part of [DevOps](/topics/devops/) and any modern software development practice. A purpose-built CI/CD platform can maximize development time by improving an organization’s productivity, increasing efficiency, and streamlining workflows through built-in automation, testing, and collaboration. As applications grow larger, the features of CI/CD can help [decrease development complexity](/blog/2022/02/22/parent-child-vs-multi-project-pipelines/). Adopting other DevOps practices — like shifting left on security and creating tighter feedback loops — helps organizations break down development silos, scale safely, and get the most out of CI/CD.

                CI/CD is important because it helps development, security, and operations teams work as efficiently and effectively as possible. It decreases tedious and time-consuming manual development work and legacy approval processes, freeing DevOps teams to be more innovative in their software development. Automation makes processes predictable and repeatable so that there is less opportunity for error from human intervention. DevOps teams gain faster feedback and can integrate smaller changes frequently to reduce the risk of build-breaking changes. Making DevOps processes continuous and iterative speeds software development lifecycles so organizations can ship more features that customers love.


                ### What is continuous integration (CI)?

                [Continuous integration](/topics/ci-cd/benefits-continuous-integration/) is the practice of integrating all your code changes into the main branch of a shared source code repository early and often, automatically testing each change when you commit or merge them, and automatically kicking off a build. With continuous integration, errors and security issues can be identified and fixed more easily, and much earlier in the development process.

                By merging changes frequently and triggering automatic testing and validation processes, you minimize the possibility of code conflict, even with multiple developers working on the same application. A secondary advantage is that you don't have to wait long for answers and can, if necessary, fix bugs and security issues while the topic is still fresh in your mind.

                Common code validation processes start with a static code analysis that verifies the quality of the code. Once the code passes the static tests, automated CI routines package and compile the code for further automated testing. CI processes should have a version control system that tracks changes so you know the version of the code used.
              image:
                image_url: /nuxt-images/cicd/g_gitlab-ci-cd.svg
                alt: Continuous integration and continuous development
              column: true
              inverted: true
      - name: 'copy'
        data:
          block:
            - hide_horizontal_rule: true
              column_size: 8
              text: |
                ### What is continuous delivery (CD)?

                Continuous delivery is a software development practice that works in conjunction with CI to automate the infrastructure provisioning and application release process.

                Once code has been tested and built as part of the CI process, CD takes over during the final stages to ensure it’s packaged with everything it needs to deploy to any environment at any time. CD can cover everything from provisioning the infrastructure to deploying the application to the testing or production environment.

                With CD, the software is built so that it can be deployed to production at any time. Then you can trigger the deployments manually or move to continuous deployment, where deployments are automated as well.

                ### What is continuous deployment?

                Continuous deployment enables organizations to deploy their applications automatically, eliminating the need for human intervention. With continuous deployment, DevOps teams set the criteria for code releases ahead of time and when those criteria are met and validated, the code is deployed into the production environment. This allows organizations to be more nimble and get new features into the hands of users faster.

                While you can do continuous integration without continuous delivery or deployment, you can’t really do CD without already having CI in place. That’s because it would be extremely difficult to be able to deploy to production at any time if you aren’t practicing CI fundamentals like integrating code to a shared repo, automating testing and builds, and doing it all in small batches on a daily basis.

                ### What is continuous testing?

                Continuous testing is a software testing practice where tests are continuously run in order to identify bugs as soon as they are introduced into the codebase. In a CI/CD pipeline, continuous testing is typically performed automatically, with each code change triggering a series of tests to ensure that the application is still working as expected. This can help to identify problems early in the development process and prevent them from becoming more difficult and costly to fix later on. Continuous testing can also provide valuable feedback to developers about the quality of their code, helping them to identify and address potential issues before they are released to production.

                In continuous testing, various types of tests are performed within the CI/CD pipeline. These can include:

                * **Unit testing**, which checks that individual units of code work as expected
                * **Integration testing**, which verifies how different modules or services within an application work together
                * **Regression testing**, which is performed after a bug is fixed to ensure that specific bug won't occur again

      - name: 'copy'
        data:
          block:
            - header: CI/CD fundamentals
              hide_horizontal_rule: true
              column_size: 8
              text: |
                There are eight fundamental elements of CI/CD that help ensure maximum efficiency for your development lifecycle. They span development and deployment. Include these fundamentals in your pipeline to improve your DevOps workflow and software delivery:

                1. **A single source repository**
                Source code management (SCM) that houses all necessary files and scripts to create builds is critical. The repository should contain everything needed for the build. This includes source code, database structure, libraries, properties files, and version control. It should also contain test scripts and scripts to build applications.

                2. **Frequent check-ins to main branch**
                Integrate code in your trunk, mainline or master branch — i.e., trunk-based development — early and often. Avoid sub-branches and work with the main branch only. Use small segments of code and merge them into the branch as frequently as possible. Don't merge more than one change at a time.

                3. **Automated builds**
                Scripts should include everything you need to build from a single command. This includes web server files, database scripts, and application software. The CI processes should automatically package and compile the code into a usable application.

                4. **Self-testing builds**
                CI/CD requires continuous testing. Testing scripts should ensure that the failure of a test results in a failed build. Use static pre-build testing scripts to check code for integrity, quality, and security compliance. Only allow code that passes static tests into the build.

                5. **Frequent iterations**
                Multiple commits to the repository results in fewer places for conflicts to hide. Make small, frequent iterations rather than major changes. By doing this, it's possible to roll changes back easily if there's a problem or conflict.

                6. **Stable testing environments**
                Code should be tested in a cloned version of the production environment. You can't test new code in the live production version. Create a cloned environment that's as close as possible to the real environment. Use rigorous testing scripts to detect and identify bugs that slipped through the initial pre-build testing process.

                7. **Maximum visibility**
                Every developer should be able to access the latest executables and see any changes made to the repository. Information in the repository should be visible to all. Use version control to manage handoffs so developers know which is the latest version. Maximum visibility means everyone can monitor progress and identify potential concerns.

                8. **Predictable deployments anytime**
                Deployments should be so routine and low-risk that the team is comfortable doing them anytime. CI/CD testing and verification processes should be rigorous and reliable, giving the team confidence to deploy updates at any time. Frequent deployments incorporating limited changes also pose lower risks and can be easily rolled back.

      - name: copy
        data:
          block:
            - header: The benefits of CI/CD implementation
              column_size: 8
              hide_horizontal_rule: true
              text: |
                Companies and organizations that adopt CI/CD tend to notice a lot of positive changes. Here are some of the benefits you can look forward to as you implement CI/CD:
                * **Happier users and customers:** Fewer bugs and errors make it into production, so your users and customers have a better experience. This leads to improved levels of customer satisfaction, improved customer confidence, and a better reputation for your organization.
                * **Accelerated time-to-value:** When you can deploy any time, you can bring products and new features to market faster. Your development costs are lower, and a faster turnaround frees your team for other work. Customers get results faster, giving your company a competitive edge.
                * **Less fire fighting:** Testing code more often, in smaller batches, and earlier in the development cycle can seriously cut down on fire drills. This results in a smoother development cycle and less team stress. Results are more predictable, and it's easier to find and fix bugs.
                * **Hit dates more reliably:** Removing deployment bottlenecks and making deployments predictable can remove a lot of the uncertainty around hitting key dates. Breaking work into smaller, manageable bites means it's easier to complete each stage on time and track progress. This approach gives plenty of time to monitor overall progress and determine completion dates more accurately.
                * **Free up developers’ time:** With more of the deployment process automated, the team has time for more rewarding projects. It's estimated that developers spend between 35% and 50% of their time testing, validating, and debugging code. By automating these processes, developers significantly improve their productivity.
                * **Less context switching:** Getting real-time feedback on their code makes it easier for developers to work on one thing at a time and minimizes their [cognitive load](https://techbeacon.com/app-dev-testing/forget-monoliths-vs-microservices-cognitive-load-what-matters). By working with small sections of code that are automatically tested, developers can debug code quickly while their minds are still fresh from programming. Finding bugs is easier because there's less code to review.
                * **Reduce burnout:** [Research shows](https://continuousdelivery.com/evidence-case-studies/#research) that CD measurably reduces deployment pain and team burnout. Developers experience less frustration and strain when working with CI/CD processes. This leads to happier and healthier employees and less burnout.
                * **Recover faster:** CI/CD makes it easier to fix issues and recover from incidents, reducing mean time to resolution (MTTR). Continuous deployment practices mean frequent small software updates so when bugs appear, it's easier to pin them down. Developers have the option of fixing bugs quickly or rolling back the change so that the customer can get back to work quickly.

      - name: copy
        data:
          block:
            - header: Why GitLab CI/CD?
              column_size: 8
              hide_horizontal_rule: true
              text: |
                In order to complete all the required fundamentals of full CI/CD, many CI platforms rely on integrations with other tools to fulfill those needs. Many organizations have to maintain costly and complicated toolchains in order to have full CI/CD capabilities. This often means maintaining a separate SCM like Bitbucket or GitHub, and connecting to a separate testing tool that connects to their CI tool, that connects to a deployment tool like Chef or Puppet, that also connects to various security and monitoring tools.

                Instead of just focusing on building great software, organizations have to also maintain and manage a complicated toolchain. GitLab is a single application for the entire DevSecOps lifecycle, meaning we fulfill all the fundamentals for CI/CD in one environment.
      - name: old-topics-cta
        data:
          title: Build, test, deploy, and monitor your code from a single application
          text: |
                We believe a single application that offers visibility across the entire
                SDLC is the best way to ensure that every development stage is included
                and optimized. When everything is under one roof, it's as easy to pinpoint
                workflow bottlenecks and evaluate the impact each element has on
                deployment speed. GitLab has CI/CD built right in, no plugins required.
          column_size: 8
          cta_one:
            text: Explore GitLab CI
            link: /features/continuous-integration/
            data_ga_name: Explore GitLab CI
            data_ga_location: body
          cta_two:
            text: Explore GitLab CD
            link: /stages-devops-lifecycle/continuous-delivery/
            data_ga_name: Explore GitLab CD
            data_ga_location: body
  components:
    - name: copy-resources
      data:
        title: CI Resources
        block:
          - text: |
                  Here’s a list of resources on CI that we find to be particularly helpful. We would love to get your recommendations on books, blogs, videos, podcasts, and other resources that tell a great story or offer valuable insight.

                  Share your favorites with us by tweeting us [@gitlab! ](https://twitter.com/gitlab)
            resources:
              webcast:
                header: Webcasts
                links:
                  - text: Mastering continuous software development
              video:
                header: Videos
                links:
                  - text: GitOps Video Playlist
                    link: https://www.youtube.com/watch?list=PL05JrBw4t0KoixDjLVMzKwVgx0eNyDOIo&v=JtZfnrwOOAw
              blog:
                header: Blogs
                links:
                  - text: Why collaboration technology is critical for GitOps
                    link: /topics/gitops/gitops-gitlab-collaboration/
                    data_ga_name: Collaboration technology GitOps
                    data_ga_location: body
                  - text: How to use GitLab and Ansible to create infrastructure as code
                    link: /blog/2019/07/01/using-ansible-and-gitlab-as-infrastructure-for-code/
                    data_ga_name: GitLab and Ansible
                    data_ga_location: body
              report:
                header: Reports
                links:
                  - text: '[Expert Panel Discussion] GitOps: The Future of Infrastructure Automation'
                    link: /why/gitops-infrastructure-automation/
                    data_ga_name: 'GitOps: The future of Infrastructure Automation'
                    data_ga_location: body
    - name: featured-media
      data:
        header: Suggested Content
        column_size: 6
        media:
          - title: Why GitLab CI/CD?
            aos_animation: fade-up
            aos_duration: 500
            text: |
                  With GitLab’s out-of-the-box CI/CD, you can spend less time maintaining and more time creating.
            link:
              text: Learn more
              href: /blog/2019/04/02/why-gitlab-ci-cd/
            image:
              url: /nuxt-images/blogimages/ci-cd-competitive-campaign-blog-cover.jpg
              alt: Competitive CI/CD
          - title: A beginner's guide to continuous integration
            aos_animation: fade-up
            aos_duration: 1000
            text: |
                  Here's how to help everyone on your team, like designers and testers, get started with GitLab CI.
            link:
              text: Learn more
              href: /blog/2018/01/22/a-beginners-guide-to-continuous-integration/
            image:
              url: /nuxt-images/blogimages/beginners-guide-to-ci.jpg
              alt: Beginner's guide tom CI
          - title: 5 Teams that made the switch to GitLab CI/CD
            aos_animation: fade-up
            aos_duration: 1500
            text: |
                  See what happened when these five teams moved on from old continuous integration and delivery solutions and switched to GitLab CI/CD.
            link:
              text: Learn more
              href: /blog/2019/04/25/5-teams-that-made-the-switch-to-gitlab-ci-cd/
            image:
              url: /nuxt-images/blogimages/ci-cd-competitive-campaign-blog-cover.jpg
              alt: Competitive CI/CD
          - title: 4 Benefits of CI/CD
            aos_animation: fade-up
            aos_duration: 1500
            text: |
                  Learn how to implement and measure a successful CI/CD pipeline strategy and help your DevOps team deliver higher quality software, faster!
            link:
              text: Learn more
              href: /blog/2019/06/27/positive-outcomes-ci-cd/
            image:
              url: /nuxt-images/blogimages/modernize-cicd.jpg
              alt: GitLab CI/CD
          - title: The business impact of CI/CD
            aos_animation: fade-up
            aos_duration: 500
            text: |
                  How a good CI/CD strategy generates revenue and keeps developers happy.
            link:
              text: Learn more
              href: /blog/2019/06/21/business-impact-ci-cd/
            image:
              url: /nuxt-images/blogimages/modernize-cicd.jpg
              alt: GitLab CI/CD
          - title: How DevOps and GitLab CI/CD enhance a frontend workflow
            aos_animation: fade-up
            aos_duration: 1000
            text: |
                  The GitLab frontend team uses DevOps and CI/CD to ensure code consistency, fast delivery, and simple automation.
            link:
              text: Learn more
              href: /blog/2018/08/09/how-devops-and-gitlab-cicd-enhance-a-frontend-workflow/
            image:
              url: /nuxt-images/blogimages/frontendworkflow.jpg
              alt: Frontend Workflow
  schema_faq:
    - question: What is CI/CD
      answer: |
        Continuous integration (CI) and continuous delivery (CD) enable DevOps
        teams to increase the speed of software development and deliver better
        quality code, faster. Continuous integration works to integrate code from
        your team in a shared repository vastly improving your deployment
        pipeline. Developers share their new code in a Merge (Pull) Request, which
        triggers a pipeline to build, test, and validate the new code before
        merging the changes in your repository. Continuous delivery deploys
        CI-validated code to your application.


        [Learn more about CI/CD](/topics/ci-cd/)
    - question: Benefits of CI/CD
      answer: |
        CI/CD automates workflows and reduces error rates within a production
        environment, which can have far-reaching impacts on not just development
        teams but throughout a whole organization.


        * More time for innovation

        * Better retention rates

        * More revenue

        * Business efficiency


        [Learn more about the benefits of CI/CD](/topics/ci-cd/)
    - question: Why Gitlab CI/CD?
      answer: |
        GitLab is a single application for the entire DevOps lifecycle, meaning
        we fulfill all the fundamentals for CI/CD in one environment.


        [Learn more about CI/CD](/topics/ci-cd/#why-gitlab-ci-cd)
