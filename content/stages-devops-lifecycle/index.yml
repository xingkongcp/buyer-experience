---
  title: Unify the DevOps lifecycle with GitLab
  description: GitLab unifies the entire DevOps lifecycle. Go from managing multiple applications to GitLab to manage every aspect of your DevOps workflow.
  side_menu:
    placeholder: Select a stage
    links:
      - text: Plan
        href: "#plan"
        data_ga_name: plan
        data_ga_location: header
      - text: Create
        href: "#create"
        data_ga_name: create
        data_ga_location: header
      - text: Verify
        href: "#verify"
        data_ga_name: verify
        data_ga_location: header
      - text: Package
        href: "#package"
        data_ga_name: Package
        data_ga_location: header
      - text: Secure
        href: "#secure"
        data_ga_name: secure
        data_ga_location: header
      - text: Release
        href: "#release"
        data_ga_name: release
        data_ga_location: header
      - text: Configure
        href: "#configure"
        data_ga_name: configure
        data_ga_location: header
      - text: Monitor
        href: "#monitor"
        data_ga_name: monitor
        data_ga_location: header
      - text: Govern
        href: "#govern"
        data_ga_name: govern
        data_ga_location: header
  call_to_action:
    title: Unify the entire DevOps lifecycle with GitLab
    subtitle: Simplify your workflow with a single application for all the stages
    body_text: |
      GitLab is The DevOps Platform that empowers organizations to maximize the overall return on software development by delivering software faster and efficiently, while strengthening security and compliance. With GitLab, every team in your organization can collaboratively plan, build, secure, and deploy software to drive business outcomes faster with complete transparency, consistency and traceability across the DevOps lifecycle.
    horizontal_rule: true

  copy_medias:
      - data:
        block:
          - header: Plan
            icon:
              name: plan-alt-2
              alt: Plan Icon
              variant: marketing
            subtitle: Regardless of your process, GitLab provides powerful planning tools to keep everyone synchronized.
            text: |
              GitLab enables portfolio planning and management through epics, groups (programs) and milestones to organize and track progress. Regardless of your methodology from Waterfall to DevOps, GitLab’s simple and flexible approach to planning meets the needs of small teams to large enterprises. GitLab helps teams organize, plan, align and track project work to ensure teams are working on the right things at the right time and maintain end to end visibility and traceability of issues throughout the delivery lifecycle from idea to production.
            image:
              image_url: /nuxt-images/home/devops-lifecycle/plan.png
              alt: Plan
            link_href: /stages-devops-lifecycle/plan/
            link_text: Learn more →
            link_variant: text
            inverted: true
            metadata:
              id_tag: plan
      - data:
        block:
          - header: Create
            icon:
              name: create-alt-2
              alt: Create Icon
              variant: marketing
            subtitle: Create, view, and manage code and project data through powerful branching tools.
            text: |
              GitLab helps teams design, develop and securely manage code and project data from a single distributed version control system to enable rapid iteration and delivery of business value. GitLab repositories provide a scalable, single source of truth for collaborating on projects and code which enables teams to be productive without disrupting their workflows.
            image:
              image_url: /nuxt-images/home/devops-lifecycle/create.png
              alt: Create
            link_href: /stages-devops-lifecycle/create/
            link_text: Learn more →
            link_variant: text
            metadata:
              id_tag: create
      - data:
        block:
          - header: Verify
            icon:
              name: verify-alt-2
              alt: Verify Icon
              variant: marketing
            subtitle: Keep strict quality standards for production code with automatic testing and reporting.
            text: |
              GitLab helps delivery teams fully embrace continuous integration to automate the builds, integration and verification of their code. GitLab’s industry leading CI capabilities enables automated testing, Static Analysis Security Testing, Dynamic Analysis Security testing and code quality analysis to provide fast feedback to developers and testers about the quality of their code. With pipelines that enable concurrent testing and parallel execution, teams quickly get insight about every commit, allowing them to deliver higher quality code faster.
            image:
              image_url: /nuxt-images/home/devops-lifecycle/verify.png
              alt: verify
            link_href: /stages-devops-lifecycle/verify/
            link_text: Learn more →
            link_variant: text
            inverted: true
            metadata:
              id_tag: verify
      - data:
        block:
          - header: Package
            icon:
              name: package-alt-2
              alt: Package Icon
              variant: marketing
            subtitle: Create a consistent and dependable software supply chain with built-in package management.
            text: |
              GitLab enables teams to package their applications and dependencies, manage containers, and build artifacts with ease. The private, secure, container and package registry are built-in and preconfigured out-of-the box to work seamlessly with GitLab source code management and CI/CD pipelines. Ensure DevOps acceleration and a faster time to market with automated software pipelines that flow freely without interruption.
            image:
              image_url: /nuxt-images/home/devops-lifecycle/package.png
              image_alt: Package
            link_href: /stages-devops-lifecycle/package/
            link_text: Learn more →
            link_variant: text
            metadata:
              id_tag: package
      - data:
        block:
          - header: Secure
            icon:
              name: secure-alt-2
              alt: Secure Icon
              variant: marketing
            subtitle: Security capabilities, integrated into your development lifecycle.
            text: |
              GitLab provides Static Application Security Testing (SAST), Dynamic Application Security Testing (DAST), Container Scanning, and Dependency Scanning to help you deliver secure applications along with license compliance.
            image:
              image_url: /nuxt-images/home/devops-lifecycle/secure.png
              image_alt: Secure
            link_href: /stages-devops-lifecycle/secure/
            link_text: Learn more →
            link_variant: text
            inverted: true
            metadata:
              id_tag: secure
      - data:
        block:
          - header: Release
            icon:
              name: release-alt-2
              alt: Release Icon
              variant: marketing
            subtitle: GitLab's integrated CD solution allows you to ship code with zero-touch, be it on one or one thousand servers.
            text: |
              GitLab helps automate the release and delivery of applications, shortening the delivery lifecycle, streamlining manual processes, and accelerating team velocity. With zero-touch Continuous Delivery (CD) built right into the pipeline, deployments can be automated to multiple environments like staging and production, and the system just knows what to do without being told - even for more advanced patterns like canary deployments. With feature flags, built-in auditing/traceability, on-demand environments, and GitLab pages for static content delivery, you'll be able to deliver faster and with more confidence than ever before.
            image:
              image_url: /nuxt-images/home/devops-lifecycle/release.png
              image_alt: Release
            link_href: /stages-devops-lifecycle/release/
            link_text: Learn more →
            link_variant: text
            metadata:
              id_tag: release
      - data:
        block:
          - header: Configure
            icon:
              name: configure-alt-2
              alt: Configure Icon
              variant: marketing
            subtitle: Configure your applications and infrastructure.
            text: |
              GitLab helps teams to configure and manage their application environments. Strong integration to Kubernetes reduces the effort needed to define and configure the infrastructure required to support your application. Protect access to key infrastructure configuration details such as passwords and login information by using ‘secret variables’ to limit access to only authorized users and processes.
            image:
              image_url: /nuxt-images/home/devops-lifecycle/configure.png
              image_alt: Configure
            link_href: /stages-devops-lifecycle/configure/
            link_text: Learn more →
            link_variant: text
            inverted: true
            metadata:
              id_tag: configure
      - data:
        block:
          - header: Monitor
            icon:
              name: monitor-alt-2
              alt: Monitor Icon
              variant: marketing
            subtitle: Help reduce the severity and frequency of incidents.
            text: |
              Get feedback and the tools to help you reduce the severity and frequency of incidents so that you can release software frequently with confidence.
            image:
              image_url: /nuxt-images/home/devops-lifecycle/monitor.png
              image_alt: Monitor
            link_href: /stages-devops-lifecycle/monitor/
            link_text: Learn more →
            link_variant: text
            metadata:
              id_tag: monitor
      - data:
        block:
          - header: Govern
            icon:
              name: protect-alt-2
              alt: Govern Icon
              variant: marketing
            subtitle: Manage security vulnerabilities, policies, and compliance across your organization.
            text: |
              GitLab helps users manage security vulnerabilities, policies, and compliance across their organization.
            image:
              image_url: /nuxt-images/home/devops-lifecycle/govern.png
              image_alt: Govern
            link_href: /stages-devops-lifecycle/govern/
            link_text: Learn more →
            link_variant: text
            inverted: true
            metadata:
              id_tag: govern
  topics_cards:
    cards:
      - category: EBOOK
        title: 'Starting and Scaling DevOps in the Enterprise'
        description: Learn more
        icon:
          name: ebook
          alt: Ebook Icon
          variant: marketing
          hex_color: '#8440C2'
        url: /resources/scaling-enterprise-devops/
        data_ga_name: 20 years of open source
        data_ga_location: topics
      - category: BLOG POST
        title: "Making the case for a DevOps platform: What data and customers say"
        description: Learn more
        icon:
          name: articles
          alt: Articles Icon
          variant: marketing
          hex_color: '#8440C2'
        url: /blog/2021/09/08/making-the-case-for-a-devops-platform-what-data-and-customers-say/
        data_ga_name: Agile Delivery
        data_ga_location: topics
      - category: WHITE PAPER
        title: Manage your toolchain before it manages you
        description: Learn more
        icon:
          name: whitepapers
          alt: Whitepapers Icon
          variant: marketing
          hex_color: '#8440C2'
        url: /resources/whitepaper-forrester-manage-your-toolchain/
        data_ga_name: DevSecOps
        data_ga_location: topics
