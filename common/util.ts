export function capitalizeFirst(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export function numberToDigitString(numbr: number) {
  return `${numbr > 9 ? numbr : `0${numbr}`}`;
}
